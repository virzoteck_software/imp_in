# -*- coding: utf-8 -*-
{
    'name': 'Import Invoice from Excel or CSV File',
    'category': 'Accounting',
    'website': '',
    'summary': '',
    'author':'virzoteck software and solutions(virzoteck@gmail.com)',
    'description': """
    
    This Module help to import bulk invoice together,which multiple product lines
    Do you want to import bulk invoices together ? Which has multiple product lines ?

Using this module you can easily do that with single click. Excel file from your supplier no need to enter them manually on odoo, you can easily import them on Odoo.

This module helps you to import customer and  invoices both with very easy excel/csv format . Additionally we provide option to choose existing odoo invoice number or import invoice number from excel file which you can select.
    """,
    'depends': [
       'account','account_invoicing'
    ],
    'data': [
        'views/import_invoice_tool.xml'
        
    ],
    'demo': [],
        "price":25,
    "currency": "EUR",
        'images': ['static/description/background.png',],
    'installable': True,
    'application': True,
}
