# -*- coding: utf-8 -*-
from odoo import fields, models, api, SUPERUSER_ID,_
import xlrd
import itertools
import base64
from odoo.exceptions import UserError, ValidationError
from datetime import datetime



class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    
    invoice_id=fields.Char("Invoice Id")
    

class ImportInvoiceTool(models.TransientModel):
    _name = "import.invoice.tool"
    
    def _default_retrive_sample_file_excel(self):
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        file_link = base_url + '/import_invoice_excelsheet/static/description/sample.xls'
        return file_link

    def _default_retrive_sample_file_csv(self):
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        file_link = base_url + '/import_invoice_excelsheet/static/description/sample.csv'
        return file_link    
    
    
    file_type = fields.Selection([('csv', 'CSV'), ('excel', 'Excel')], required=True, default='excel')
    invoice_type = fields.Selection([('customer', 'Customer'), ('supplier', 'Supplier')], required=True, default='customer')
    sequence_option = fields.Selection([('exel_csv_number', 'Use Excel/CSV sequence Number'), ('system_genrated_number', 'Use System Default Sequence Number')], required=True, default='exel_csv_number')
    account_option = fields.Selection([('account_from_config', 'Use Account from configuration product/property'), ('use_account_from_excel', 'Use Account From Excel/CSV')], required=True, default='account_from_config')
    import_product_by = fields.Selection([('name', 'Name'), ('code', 'Code'),('barcode','Barcode')], required=True, default='name')
    file=fields.Binary("File")
    invoice_stage_option = fields.Selection([('draft_in', 'Import Draft Invoice'), ('validate_auto', 'Validate invoice Automatically with import')], required=True, default='draft_in')
    sample_file_link_excel = fields.Char("Download sample File for excel Format",
                                         default=_default_retrive_sample_file_excel, readonly=True)
    sample_file_link_csv = fields.Char("Download sample File for CSV Format", default=_default_retrive_sample_file_csv,
                                       readonly=True)


    @api.multi
    def import_invoices_by_excel(self):
        invoice_obj=self.env['account.invoice']
        partner_obj=self.env['res.partner']
        product_obj=self.env['product.product']
        account_obj=self.env['account.account']
        account_invoice_line_obj=self.env['account.invoice.line']        
        user_obj=self.env['res.users']
        tax_obj=self.env['account.tax']
        
        file=base64.b64decode(self.file)

        wb = xlrd.open_workbook(file_contents=file)
        
#        try:
            #import Sheet 1
        sheet = wb.sheet_by_index(0)
        row_count=0
        invoice_id_list=[]
        for row in list(map(sheet.row, range(sheet.nrows))):
            row_count+=1
            if row_count==1:
                continue 
                
                
            invoice_id=invoice_obj.search([('invoice_id','=',row[0].value)])
            if not len(invoice_id):
                partner_id=partner_obj.search([('name','=',row[1].value)])
                if not len(partner_id):
                    partner_id=partner_obj.create({'name':row[1].value})
                    partner_id=partner_id.id
                else:
                    partner_id=partner_id[0].id


                if self.import_product_by=='name':
                    product_id=product_obj.search([('name','=',row[2].value)])
                    if not len(product_id):
                        product_id=product_obj.create({'name':row[2].value}).id
                    else:
                        product_id=product_id[0].id
                        
                if self.import_product_by=='code':
                    product_id=product_obj.search([('default_code','=',row[2].value)])
                    if not len(product_id):
                        product_id=product_obj.create({'name':row[2].value,'default_code':row[2].value}).id
                    else:
                        product_id=product_id[0].id   
                        
                if self.import_product_by=='barcode':
                    product_id=product_obj.search([('barcode','=',row[2].value)])
                    if not len(product_id):
                        product_id=product_obj.create({'barcode':row[2].value}).id
                    else:
                        product_id=product_id[0].id                                         


                user_id=user_obj.search([('name','=',row[8].value)])
                print ('user_id========%s'% row[8].value)
                if not len(user_id):
                    user_id=SUPERUSER_ID
                else:
                    user_id=user_id[0].id
                print ('user_id========%s'% row[10].value)    
                ndate=datetime.strptime(row[10].value, "%Y/%m/%d")
                if self.invoice_type=='customer':
                    in_type='out_invoice'
                else:
                    in_type='in_invoice'

                vals={'date_invoice':ndate,'invoice_id':row[0].value,'user_id':user_id,'partner_id':partner_id,'type':in_type,'currency_id': self.env.user.company_id.currency_id.id,'name':'-'}
                if self.sequence_option=='excel_csv_number':
                    vals.update({'number':row[0].value})
                invoice = invoice_obj.new(vals)
                invoice._onchange_partner_id()
                vals.update({'account_id': invoice.account_id.id})
                invoice_id = invoice_obj.create(vals)
                invoice_id_list.append(invoice_id)
                

                
                print ('invoice_line========%s'% row[10].value)
#                excel_date=int(row[10].value)
#                dt = datetime.fromordinal(datetime(1900, 1, 1).toordinal() + excel_date - 2)
#                tt = dt.timetuple()

#                print ('invoice_line========%f'% tt)
                account_id=account_obj.search([('code','=',int(row[3].value))])
                print ('invoice_line========%s'% row[3].value)  
                
                if  len(account_id):
                    account_id=account_id[0].id
                else:
                    account_id=invoice_id.journal_id.default_debit_account_id.id
                
                if row[9].value.find(',')!=-1:
                    tax_data=row[9].value.split(',')
                else:
                    tax_data=[row[9].value]
                tax_ids=[]
                for tax_rec in tax_data:
                    tax_id=tax_obj.search([('name','=',tax_rec)])
                    if  len(tax_id):
                        tax_id=tax_id[0].id
                        tax_ids.append(tax_id)

                
                account_invoice_line_obj.create({'product_id':product_id,
                                                  'invoice_id':invoice_id.id,
                                                 'name':row[6].value,
                                                 'account_id':account_id,
                                                 'quantity':row[4].value,
                                                 'price_unit':row[7].value,
                                                 'invoice_line_tax_ids': [(6, 0, tax_ids)]
                                                })  
            else:           
                product_id=product_obj.search([('name','=',row[2].value)])
                if not len(product_id):
                    product_id=product_obj.create({'name':row[2].value}).id
                else:
                    product_id=product_id[0].id

                account_id=account_obj.search([('code','=',int(row[3].value))])
                if not len(account_id):
                    account_id=invoice_id.journal_id.default_debit_account_id.id
                else:
                    account_id=account_id[0].id
                    
                
                if row[9].value.find(',')!=-1:
                    tax_data=row[9].value.split(',')
                else:
                    tax_data=[row[9].value]
                tax_ids=[]
                for tax_rec in tax_data:
                    tax_id=tax_obj.search([('name','=',tax_rec)])
                    if  len(tax_id):
                        tax_id=tax_id[0].id
                        tax_ids.append(tax_id)     
                account_invoice_line_obj.create({'product_id':product_id,
                                                  'invoice_id':invoice_id[0].id,
                                                 'name':row[6].value,
                                                 'account_id':account_id,
                                                 'quantity':row[4].value,
                                                 'price_unit':row[7].value,
                                                 'invoice_line_tax_ids': [(6, 0, tax_ids)]
                                                })  
            print ('user_id========%s'% self.sequence_option)    
            if self.invoice_stage_option=='validate_auto':
                for invoice_id in invoice_id_list:
                    invoice_id.action_invoice_open()
                    if self.sequence_option=='exel_csv_number':
                        invoice_id.write({'number':invoice_id.invoice_id})
                    
#        except Exception as e:
#            raise UserError(_("%s") % str(e))    
        return True
    @api.multi
    def import_invoices_by_csv(self):
        invoice_obj=self.env['account.invoice']
        partner_obj=self.env['res.partner']
        product_obj=self.env['product.product']
        account_obj=self.env['account.account']
        account_invoice_line_obj=self.env['account.invoice.line']        
        user_obj=self.env['res.users']
        tax_obj=self.env['account.tax']

        file_data = str(base64.b64decode(self.file))
        print ('row========%s'% file_data.split('\\n')[0])               
        row_count = 0
        #        try:
        invoice_id_list=[]        
        for row_dataz in file_data.split('\\n'):
            row_count+=1
            if row_count==1:
                continue    
            row_data=row_dataz.split(',')
            if len(row_data)!=11:
                continue
            
            print ('row====aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa====')                
            invoice_id=invoice_obj.search([('invoice_id','=',row_data[0])])
            if not len(invoice_id):
                partner_id=partner_obj.search([('name','=',row_data[1])])
                if not len(partner_id):
                    partner_id=partner_obj.create({'name':row_data[1]})
                    partner_id=partner_id.id
                else:
                    partner_id=partner_id[0].id


                if self.import_product_by=='name':
                    product_id=product_obj.search([('name','=',row_data[2])])
                    if not len(product_id):
                        product_id=product_obj.create({'name':row_data[2]}).id
                    else:
                        product_id=product_id[0].id
                        
                if self.import_product_by=='code':
                    product_id=product_obj.search([('default_code','=',row_data[2])])
                    if not len(product_id):
                        product_id=product_obj.create({'name':row_data[2],'default_code':row_data[2]}).id
                    else:
                        product_id=product_id[0].id   
                        
                if self.import_product_by=='barcode':
                    product_id=product_obj.search([('barcode','=',row_data[2])])
                    if not len(product_id):
                        product_id=product_obj.create({'name':row_data[2],'barcode':row_data[2]}).id
                    else:
                        product_id=product_id[0].id                                         


                user_id=user_obj.search([('name','=',row_data[8])])
                print ('user_id========%s'% row_data[8])
                if not len(user_id):
                    user_id=SUPERUSER_ID
                else:
                    user_id=user_id[0].id
                print ('user_id========%s'% row_data[10])    
                ndate=datetime.strptime(row_data[10], "%Y/%m/%d")
                if self.invoice_type=='customer':
                    in_type='out_invoice'
                else:
                    in_type='in_invoice'

                vals={'date_invoice':ndate,'invoice_id':row_data[0],'user_id':user_id,'partner_id':partner_id,'type':in_type,'currency_id': self.env.user.company_id.currency_id.id,'name':'-'}
                if self.sequence_option=='excel_csv_number':
                    vals.update({'number':row_data[0]})
                invoice = invoice_obj.new(vals)
                invoice._onchange_partner_id()
                vals.update({'account_id': invoice.account_id.id})
                invoice_id = invoice_obj.create(vals)
                invoice_id_list.append(invoice_id)
                

                
                print ('invoice_line========%s'% row_data[10])
#                excel_date=int(row[10].value)
#                dt = datetime.fromordinal(datetime(1900, 1, 1).toordinal() + excel_date - 2)
#                tt = dt.timetuple()

#                print ('invoice_line========%f'% tt)
                account_id=account_obj.search([('code','=',int(row_data[3]))])
                print ('invoice_line========%s'% row_data[3])  
                
                if  len(account_id):
                    account_id=account_id[0].id
                else:
                    account_id=invoice_id.journal_id.default_debit_account_id.id
                
                if row_data[9].find(',')!=-1:
                    tax_data=row_data[9].split(',')
                else:
                    tax_data=[row_data[9]]
                tax_ids=[]
                for tax_rec in tax_data:
                    tax_id=tax_obj.search([('name','=',tax_rec)])
                    if  len(tax_id):
                        tax_id=tax_id[0].id
                        tax_ids.append(tax_id)

                
                account_invoice_line_obj.create({'product_id':product_id,
                                                  'invoice_id':invoice_id.id,
                                                 'name':row_data[6],
                                                 'account_id':account_id,
                                                 'quantity':row_data[4],
                                                 'price_unit':row_data[7],
                                                 'invoice_line_tax_ids': [(6, 0, tax_ids)]
                                                })  
            else:           
                product_id=product_obj.search([('name','=',row_data[2])])
                if not len(product_id):
                    product_id=product_obj.create({'name':row_data[2]}).id
                else:
                    product_id=product_id[0].id

                account_id=account_obj.search([('code','=',int(row_data[3]))])
                if not len(account_id):
                    account_id=invoice_id.journal_id.default_debit_account_id.id
                else:
                    account_id=account_id[0].id
                    
                
                if row_data[9].find(',')!=-1:
                    tax_data=row_data[9].split(',')
                else:
                    tax_data=[row_data[9]]
                tax_ids=[]
                for tax_rec in tax_data:
                    tax_id=tax_obj.search([('name','=',tax_rec)])
                    if  len(tax_id):
                        tax_id=tax_id[0].id
                        tax_ids.append(tax_id)     
                account_invoice_line_obj.create({'product_id':product_id,
                                                  'invoice_id':invoice_id[0].id,
                                                 'name':row_data[6],
                                                 'account_id':account_id,
                                                 'quantity':row_data[4],
                                                 'price_unit':row_data[7],
                                                 'invoice_line_tax_ids': [(6, 0, tax_ids)]
                                                })  
            print ('user_id========%s'% self.sequence_option)    
            if self.invoice_stage_option=='validate_auto':
                for invoice_id in invoice_id_list:
                    invoice_id.action_invoice_open()
                    if self.sequence_option=='exel_csv_number':
                        invoice_id.write({'number':invoice_id.invoice_id})
                    
        return True
    
    def import_invoices(self):
        if self.file_type=='csv':
            self.import_invoices_by_csv()
        else:
            self.import_invoices_by_excel()
        return True
    
